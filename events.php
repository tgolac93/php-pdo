<?php
if(!isset($_SESSION)) {
     session_start();
}
include 'inc/redirect/Redirect.php';
include 'inc/template_parts/Header.php';
include 'inc/template_parts/Footer.php';
include 'inc/conn/DBConn.php';
include 'settings.config';
$conn = new DBConn($conf);
$username = (string) $_SESSION['username'];
$role = intval((string) $_SESSION['role']);
$page_name = "Events";

$redirect = new Redirect();
if (!isset($_SESSION['login']) || $_SESSION['login'] == "") {
    $redirect->redirect_to("login");
} 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Events</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <?php
        $header = new Header($role, $username, $page_name);
        echo $header->render();
        ?>
        <main>
            <section id="events">
                <h2>Events View</h2>
                <?php
                include 'inc/dynamic/TableMaker.php';
                $table_maker = new TableMaker($conn, $role);

                include 'inc/fetch/Event.php';
                echo $table_maker->getEventList();
                ?>
            </section>
        </main>
        <?php
        $footer = new Footer();
        echo $footer->render();
        ?>
    </body>

</html>