<?php
if (!isset($_SESSION)) {
    session_start();
}
include 'inc/redirect/Redirect.php';
include 'inc/template_parts/Header.php';
include 'inc/template_parts/Footer.php';
include 'inc/conn/DBConn.php';
include 'settings.config';
include 'inc/dynamic/TableMaker.php';
$conn = new DBConn($conf);
$user_id = intval((string) $_SESSION['user_id']);
$username = (string) $_SESSION['username'];
$role = intval((string) $_SESSION['role']);
$page_name = "Registrations";

$redirect = new Redirect();
if (!isset($_SESSION['login']) || $_SESSION['login'] == "") {
    $redirect->redirect_to("login");
} else if ($role > 2) {
    $redirect->redirect_to("events");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Events</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <?php
        $header = new Header($role, $username, $page_name);
        echo $header->render();
        ?>
        <main>
            <section id="sessions">
                <h2>Sessions</h2>
                <?php
                $table_maker = new TableMaker($conn, $role);
                include 'inc/fetch/Session.php';
                echo $table_maker->getSessionListByAttendee($user_id);
                ?>
            </section>


            <section id="events__attendee">
                <h2>Events - Attendee</h2>
                <?php
                include 'inc/fetch/Event_Paid.php';
                echo $table_maker->getEventListByAttendee($user_id);
                ?>
            </section>

        </main>    
            <?php
            $footer = new Footer();
            echo $footer->render();
            ?>
    </body>

</html>