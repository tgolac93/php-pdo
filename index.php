<?php
if(!isset($_SESSION)) {
     session_start();
}
require 'inc/redirect/Redirect.php';
$redirect = new Redirect();
if (isset($_SESSION["login"]) && $_SESSION["login"] === true) {
    
    if(intval((string)$_SESSION["role"]) > 3){
        $redirect->redirect_to('events');
    }
    else{
        $redirect->redirect_to("admin");
    }
    exit;
}
elseif (!isset($_SESSION['login']) || $_SESSION['login'] == "") {
    $redirect->redirect_to("login");
    exit;
}

