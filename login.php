<?php
if (!isset($_SESSION)) {
    session_start();

}
include 'inc/fetch/Attendee.php';
require_once 'settings.config';
require_once 'inc/conn/DBConn.php';

if (isset($_SESSION["login"]) && $_SESSION["login"] === true) {
    include 'inc/redirect/Redirect.php';
    $redirect = new Redirect();
    $redirect->redirect_to("index");
    exit;
}
$error = false;

if (isset($_POST['username'])) {

    $data = array();
    $user = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
    $pass = hash('sha256', filter_var($_POST['password'], FILTER_SANITIZE_STRING));
    if (empty($user) || empty($pass)) {
        $error = true;
        $errorMessage = "You need to input both username and password!";
    }

    $conn = new DBConn($conf);
    $conn->connect();
    $statement = $conn->runQuery("Select attendee.idattendee as id, attendee.name as name, attendee.password as password, attendee.role as role From attendee where name = :name and password = :password", "Attendee");
    $statement->bindParam(':name', $user);
    $statement->bindParam(':password', $pass);
    $statement->execute();

    while ($result_row = $statement->fetch()) {
        $data[] = $result_row;
    }

    if (count($data) > 0) {
        foreach ($data as $row) {
            $_SESSION['login'] = true;
            $_SESSION['user_id'] = $row->getId();
            $_SESSION['username'] = $row->getName();
            $_SESSION['role'] = $row->getRole();
        }
    } else {
        $error = true;
        $errorMessage = "Invalid username and/or password.";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <section class='form__section'>
            <div class='form__container'>
                <form class='form' method='POST' action='' novalidate>

                    <h1 class="form__heading">Login</h1>
                    <?php
                    if ($error) {
                        echo "<p class='error-message'>$errorMessage</p>";
                    }
                    ?>
                    <input type='text' placeholder='username' name='username' />
                    <input type='password' placeholder='password' name='password' />
                    <input type='submit' value='SUBMIT'/>
                </form>                
            </div>
        </section>
    </body>
</html>