<?php
if (!isset($_SESSION)) {
    session_start();
}
include 'inc/redirect/Redirect.php';
include 'inc/template_parts/Header.php';
include 'inc/template_parts/Footer.php';
include 'inc/conn/DBConn.php';
include 'settings.config';
include 'inc/dynamic/TableMaker.php';
$conn = new DBConn($conf);
$username = (string) $_SESSION['username'];
$role = intval((string) $_SESSION['role']);
$user_id = intval((string) $_SESSION['user_id']);
$page_name = "Admin";

$redirect = new Redirect();
if (!isset($_SESSION['login']) || $_SESSION['login'] == "") {
    $redirect->redirect_to("login");
} else if ($role > 2) {
    $redirect->redirect_to("events");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Events</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <?php
        $header = new Header($role, $username, $page_name);
        echo $header->render();
        ?>
        <main>
            <?php $table_maker = new TableMaker($conn, $role); ?>
            <?php if ($role <= 2): ?>
                <section id ="events__manager">
                    <h2>Events - Manager</h2>
                    <?php
                    include 'inc/fetch/Event_Paid.php';
                    echo $table_maker->getEventListByManager($user_id);
                    ?>
                </section>

                <section id="sessions__manager">
                    <h2>Sessions - Manager</h2>
                    <?php
                    include 'inc/fetch/Session.php';
                    echo $table_maker->getSessionListByManager($user_id);
                    ?>
                </section>



                <section id="manager__attendees">
                    <h2>Attendees of Manager's events</h2>
                    <?php
                    include 'inc/fetch/Manager_Attendee.php';
                    echo $table_maker->getEventAttendeesByManager($user_id);
                    ?>
                </section>
            <?php endif; ?>

            <?php if ($role == 1): ?>
            <section id="admin-users">
                <h2>Admin - Users</h2>
                <?php
                include 'inc/fetch/Attendee.php';
                echo $table_maker->getUserList();
                ?>
                
            </section>
            <section id="admin-events">
                <h2>Admin - Events</h2>
                <?php
                include 'inc/fetch/Event.php';
                echo $table_maker->getEventList(true);
                ?>             
            </section>
            
            <section id="admin-sessions">
                <h2>Admin - Sessions</h2>
                <?php
                //include 'inc/fetch/Session.php';
                echo $table_maker->getSessionList();
                ?>             
            </section>
            
            <section id="admin-venues">
                <h2>Admin - Venues</h2>
                <?php
                include 'inc/fetch/Venue.php';
                echo $table_maker->getVenueList();
                ?>             
            </section>
            
            <section id='admin-attendee-events'>
                <h2>Admin - Attendee Event</h2>
                <?php
                include 'inc/fetch/Attendee_Event.php';
                echo $table_maker->getAttendeeEventList();
                ?>
            </section>
            
            <section id='admin-attendee-sessions'>
                <h2>Admin - Attendee Session</h2>
                <?php
                include 'inc/fetch/Attendee_Session.php';
                echo $table_maker->getAttendeeSessionList();
                ?>
            </section>
            
             <section id='admin-manager-events'>
                <h2>Admin - Manager Event</h2>
                <?php
                include 'inc/fetch/Manager_Event.php';
                echo $table_maker->getManagerEventList();
                ?>
            </section>
            <?php
            endif;
            
            ?>

        </main>
        <?php
        $footer = new Footer();
        echo $footer->render();
        ?>
    </body>

</html>