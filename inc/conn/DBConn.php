<?php

/**
 * A main hub for all the necessary database functionalities like connection, 
 * closing the connection and CRUD OPERATIONS
 *
 * @author Nikola Bobicanec
 */
class DBConn {

    //an external associative array
    protected $config;
    //database connection variable    
    public $con;

    /**
     * Takes configuration file and stores in to the $config, 
     * than uses the getConnection function to connect to database with success
     * @param array $config
     */
    public function __construct(array $config) {
        $this->_config = $config;
        $this->connect();
    }

    public function __destruct() {
        $this->con = null;
    }

    /**
     * Connects to the database PDO
     */
    public function connect() {
        if ($this->con == null) {
            $credentials = filter_var($this->_config['db_driver'], FILTER_SANITIZE_SPECIAL_CHARS) .
                    ":host=" . filter_var($this->_config['db_host'], FILTER_SANITIZE_SPECIAL_CHARS) .
                    ";dbname=" . filter_var($this->_config['db_dbname'], FILTER_SANITIZE_SPECIAL_CHARS);

            $username = filter_var($this->_config['db_username'], FILTER_SANITIZE_SPECIAL_CHARS);
            $password = filter_var($this->_config['db_password'], FILTER_SANITIZE_SPECIAL_CHARS);

            try {
                $this->con = new PDO($credentials, $username, $password);
                $this->con->setAttribute(PDO::ATTR_ORACLE_NULLS,PDO::NULL_TO_STRING);
            } catch (PDOException $ex) {
                echo __LINE__ . $ex->getMessage();
            }
        }
    }


    /**
     * Returns the results of Select and becomes assoc array
     * @param type $query
     */
    public function runQuery($query, $className="") {
        try {
            $statement = $this->con->prepare($query);
            $statement->setFetchMode(PDO::FETCH_CLASS,$className);
            
            return $statement;
        } catch (PDOException $ex) {
            echo __LINE__ . $ex->getMessage();
        }
    }

}
