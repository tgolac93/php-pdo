<?php
class Form {
    private $form_type;
    public function __construct($form_type) {
        $this->form_type = form_type;
    }
    
    public function importForm()
    {
        return "includes/forms/{$form_type}.php";
    }
}