<?php

class Footer {

    function render() {
        ?>
        <footer class="footer"><p>&copy; Nikola Bobicanec <?php echo date("d/m/Y"); ?></p></footer>
        <?php
    }

}
