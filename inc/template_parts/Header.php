<?php

class Header {

    private $role;
    private $name;
    private $page;

    public function __construct($role, $name, $page) {
        $this->role = $role;
        $this->name = $name;
        $this->page = $page;
    }

    public function render() {
        $result = "<header class='header'>"
                . "<div class='header__title'>"
                . "<h1>{$this->page}</h1>"
                . "</div>"
                . "<nav class='navbar'>";
        
        if ($this->role <= 2) {
            $result .= "<a href='admin.php'>Admin</a>";
        }

        $result .= "<a href='events.php'>Events</a>"
                . "<a href='registrations.php'>Registrations</a>"
                . "<a href='logout.php'>Log out, {$this->name}</a>"
                . "</nav>"
                . "</header>";

        return $result;
    }

}
