<?php

class TableMaker {

    private $conn;
    private $role;

    public function __construct($conn, $role) {
        $this->conn = $conn;
        $this->role = $role;
    }

    public function getEventList($editable = false) {
        $colspan = 5;
        if ($editable) {
            $colspan = 6;
        }

        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Events</th></tr><tr><th>Name</th><th>Date Start</th><th>Date End</th><th>Number Allowed</th><th>Venue</th>";
        if ($editable) {
            $resultTable .= "<th>Action</th>";
        }
        $resultTable .= "</tr>";
        $statement = $this->conn->runQuery("Select event.idevent as id, event.name, event.datestart as dateStart, event.dateend as dateEnd, event.numberallowed as numberAllowed, venue.name as venue from venue inner join event on venue.idvenue = event.venue ", "Event");
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;
        foreach ($data as $row) {
            //print_r($row);
            $resultTable .= "<tr>"
                    . "<td id='event__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='event__datestart_{$i}'>{$row->getDateStart()}</td>"
                    . "<td id='event_dateend_{$i}'>{$row->getDateEnd()}</td>"
                    . "<td id='event_numberallowed_{$i}'>{$row->getNumberAllowed()}</td>"
                    . "<td id=event__venue_{$i}>{$row->getVenue()}</td>"
            ;

            if ($editable) {
                $resultTable .= "<td><a href='deleteData?type=eventDelete&event_id={$row->getId()}'>Delete</a></td></tr>";
            }
            $resultTable .= "</tr>";
            $i = $i + 1;
        }

        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>";
        if ($editable) {
            $resultTable .= "<tr><td colspan='{$colspan}'><a href='addData.php?type=managerEventHandler'>Add Event</a></td></tr>";
        }
        $resultTable .= "</table>";

        return $resultTable;
    }

    public function getEventListByManager($user_id) {

        $colspan = 7;


        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Manager - Events</th></tr><tr><th>Name</th><th>Date Start</th><th>Date End</th><th>Number Allowed</th><th>Venue</th>";

        $resultTable .= "<th>Action</th>";

        $resultTable .= "</tr>";
        $statement = $this->conn->runQuery("select event.idevent as id, event.name as name, event.datestart as dateStart, event.dateend as dateEnd, event.numberallowed as numberAllowed, venue.name as venue from manager_event inner join event on manager_event.event = event.idevent inner join venue on event.venue = venue.idvenue inner join attendee on manager_event.manager = attendee.idattendee where attendee.idattendee = :userid", "Event_Paid");

        $statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;
        foreach ($data as $row) {
            //print_r($row);
            $resultTable .= "<tr>"
            . "<td id='event__name_{$i}'>{$row->getName()}</td>"
            . "<td id='event__datestart_{$i}'>{$row->getDateStart()}</td>"
            . "<td id='event_dateend_{$i}'>{$row->getDateEnd()}</td>"
            . "<td id='event_numberallowed_{$i}'>{$row->getNumberAllowed()}</td>"
            . "<td id=event__venue_{$i}>{$row->getVenue()}</td>"
            . "<td><a href='deleteData?type=eventDelete&event_id={$row->getId()}'>Delete</a></td></tr>";


            $resultTable .= "</tr>";
            $i = $i + 1;
        }

        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=managerEventHandler'>Add Event</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getEventListByAttendee($user_id) {

        $colspan = 7;


        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Attendee - Events</th></tr><tr><th>Name</th><th>Date Start</th><th>Date End</th><th>Number Allowed</th><th>Venue</th><th>Paid</th>";

        $resultTable .= "<th>Action</th>";

        $resultTable .= "</tr>";
        $statement = $this->conn->runQuery("select event.idevent as id, event.name as name, event.datestart as dateStart, event.dateend as dateEnd, event.numberallowed as numberAllowed, venue.name as venue, attendee_event.paid from attendee_event inner join event on attendee_event.event = event.idevent inner join venue on event.venue = venue.idvenue inner join attendee on attendee_event.attendee = attendee.idattendee where attendee.idattendee = :userid", "Event_Paid");

        $statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;
        foreach ($data as $row) {
            //print_r($row);
            $resultTable .= "<tr>"
                    . "<td id='event__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='event__datestart_{$i}'>{$row->getDateStart()}</td>"
                    . "<td id='event_dateend_{$i}'>{$row->getDateEnd()}</td>"
                    . "<td id='event_numberallowed_{$i}'>{$row->getNumberAllowed()}</td>"
                    . "<td id=event__venue_{$i}>{$row->getVenue()}</td>"
                    . "<td id=event__venue_{$i}>{$row->getPaid()}</td>"
                    . "<td><a href='deleteData?type=attendeeEvent&event={$row->getId()}&user={$user_id}'>Delete</a></td></tr>";

            $resultTable .= "</tr>";
            $i = $i + 1;
        }

        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=attendeeEventHandler'>Add Event</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getVenueList() {
        $colspan = 3;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Sessions</th></tr><tr><th>Name</th><th>Capacity</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("SELECT venue.idvenue as id, venue.name as name, venue.capacity as capacity FROM venue", "Venue");
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);

        $i = 1;
        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='venue__name'>{$row->getName()}</td>"
                    . "<td id='venue__capacity'>{$row->getCapacity()}</td>"
                    . "<td><button name='venue__edit' value='venue_edit' id='venue_edit_{$i}' class='edit'>Edit</button><button name='venue_delete' value='venue_delete' id='venue_delete_{$i}' class='delete'>Delete</button></td>"
                    . "</tr>";
        }

        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=adminVenueHandler'>Add Venue</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getSessionList() {
        $colspan = 6;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Sessions</th></tr><tr><th>Name</th><th>Number Allowed</th><th>Event</th><th>Date Start</th><th>Date End</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("SELECT session.idsession as id, session.name, session.numberallowed as numberAllowed, session.event as event, session.startdate as startDate, session.enddate as endDate from session ", "Session");
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);

        $i = 1;
        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='session__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='session__numberallowed_{$i}'>{$row->getNumberAllowed()}</td>"
                    . "<td id='session__event_{$i}'>{$row->getEvent()}</td>"
                    . "<td id='session__startdate_{$i}'>{$row->getStartDate()}</td>"
                    . "<td id='session__name_{$i}'>{$row->getEndDate()}</td>"
                    . "<td><a href='deleteData?type=deleteSession&session_id={$row->getId()}'>Delete</a></td>"
                    . "</tr>";
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=attendeeSessionHandler'>Add Session</a></td></tr>"
                . "</table>";
        return $resultTable;
    }

    public function getSessionListByAttendee($user_id) {
        $colspan = 6;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Sessions</th></tr><tr><th>Name</th><th>Number Allowed</th><th>Event</th><th>Date Start</th><th>Date End</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("SELECT session.idsession as id,session.name as name, session.numberallowed as numberAllowed, event.name as event, session.startdate as startDate, session.enddate as endDate FROM attendee_session inner join session on session.idsession = attendee_session.session inner join event on session.event = event.idevent where attendee_session.attendee = :userid ;"
                , "Session");
        $statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);

        $i = 1;
        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='session__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='session__numberallowed_{$i}'>{$row->getNumberAllowed()}</td>"
                    . "<td id='session__event_{$i}'>{$row->getEvent()}</td>"
                    . "<td id='session__startdate_{$i}'>{$row->getStartDate()}</td>"
                    . "<td id='session__name_{$i}'>{$row->getEndDate()}</td>"
                    . "<td><a href='deleteData?type=attendeeSession&session={$row->getId()}&user={$user_id}'>Delete</a></td>"
                    . "</tr>";
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=attendeeSessionHandler'>Add Session</a></td></tr>"
                . "</table>";
        return $resultTable;
    }

    public function getSessionListByManager($user_id) {
        $colspan = 6;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Manager - Sessions</th></tr><tr><th>Name</th><th>Number Allowed</th><th>Event</th><th>Date Start</th><th>Date End</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("SELECT session.idsession as id, session.name as name, session.numberallowed as numberAllowed, event.name as event, session.startdate as startDate, session.enddate as endDate from session inner join event on session.event = event.idevent inner join manager_event on manager_event.event = event.idevent where manager_event.manager = :userid ;"
                , "Session");
        $statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);

        $i = 1;
        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='session__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='session__numberallowed_{$i}'>{$row->getNumberAllowed()}</td>"
                    . "<td id='session__event_{$i}'>{$row->getEvent()}</td>"
                    . "<td id='session__startdate_{$i}'>{$row->getStartDate()}</td>"
                    . "<td id='session__name_{$i}'>{$row->getEndDate()}</td>"
                    . "<td><a href='deleteData?type=deleteSession&session_id={$row->getId()}'>Delete</a></td>"
                    . "</tr>";
            $i = $i + 1;
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=managerSessionHandler'>Add Session</a></td></tr>"
                . "</table>";
        return $resultTable;
    }

    public function getEventAttendeesByManager($user_id) {
        $colspan = 5;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Manager - Event Attendees</th></tr>"
                . "<tr>"
                . "<th>Name</th><th>Role</th><th>Event</th><th>Paid</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("select attendee_event.event as eventId, attendee.idattendee as id, attendee.name as name, role.name as role, event.name as event ,attendee_event.paid from role inner join attendee on attendee.role = role.idrole inner join attendee_event on attendee_event.attendee = attendee.idattendee inner join event on attendee_event.event = event.idevent inner join manager_event on event.idevent = manager_event.event where manager_event.manager = :userid", "Manager_Attendee");
        $statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;
        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='event-attendee__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='event-attendee__role_{$i}'>{$row->getRole()}</td>"
                    . "<td id='event-attendee__event_{$i}'>{$row->getEvent()}</td>"
                    . "<td id='event-attendee__name_{$i}'>{$row->getPaid()}</td>"
                    . "<td><a href='deleteData?type=attendeeEvent&event={$row->getEventId()}&user={$row->getId()}'>Delete</a</td>"
                    . "</tr>";
            $i = $i + 1;
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=managerAttendeeHandler'>Add Attendee</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getManagerEventList() {
        $colspan = 3;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Manager Event</th></tr>"
                . "<tr><th>Manager</th><th>Event</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("select attendee.name as manager, event.name as event from manager_event inner join attendee on attendee.idattendee = manager_event.manager inner join event on event.idevent = manager_event.event ", 'Manager_Event');
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;

        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='attendee__name_{$i}'>{$row->getManager()}</td>"
                    . "<td id='attendee__event_{$i}'>{$row->getEvent()}</td>"
                    . "<td><button name='user__edit' value='user_edit' id='user__edit_{$i}' class='edit'>Edit</button><button name='user__delete' value='user__delete' id='user__delete{$i}' class='delete'>Delete</button></td>"
                    . "</tr>";
            $i = $i + 1;
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=adminManagerEventHandler'>Add User</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getAttendeeSessionList() {
        $colspan = 3;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Attendee Session</th></tr>"
                . "<tr><th>Attendee</th><th>Session</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("select session.name as session, attendee.name as attendee from attendee_session inner join session on attendee_session.session = session.idsession inner join attendee on attendee.idattendee = attendee_session.attendee ", 'Attendee_Session');
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;

        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='attendee__name_{$i}'>{$row->getAttendee()}</td>"
                    . "<td id='attendee__event_{$i}'>{$row->getSession()}</td>"
                    . "<td><button name='user__edit' value='user_edit' id='user__edit_{$i}' class='edit'>Edit</button><button name='user__delete' value='user__delete' id='user__delete{$i}' class='delete'>Delete</button></td>"
                    . "</tr>";
            $i = $i + 1;
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=adminAttendeeSessionHandler'>Add User</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getAttendeeEventList() {
        $colspan = 4;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Attendee Event</th></tr>"
                . "<tr><th>Attendee</th><th>Event</th><th>Paid</th><th>Action</th></tr>";
        $statement = $this->conn->runQuery("SELECT attendee.name as attendee , event.name as event, attendee_event.paid as paid from attendee inner join attendee_event on attendee_event.attendee = attendee.idattendee inner join event on event.idevent = attendee_event.event ", 'Attendee_Event');
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;

        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='attendee__name_{$i}'>{$row->getAttendee()}</td>"
                    . "<td id='attendee__event_{$i}'>{$row->getEvent()}</td>"
                    . "<td id='attendee__paid_{$i}'>{$row->getPaid()}</td>"
                    . "<td><button name='user__edit' value='user_edit' id='user__edit_{$i}' class='edit'>Edit</button><button name='user__delete' value='user__delete' id='user__delete{$i}' class='delete'>Delete</button></td>"
                    . "</tr>";
            $i = $i + 1;
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=adminAttendeeEventHandler'>Add Attendee - Event</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

    public function getUserList() {
        $colspan = 3;
        $resultTable = "<table><tr><th class='headTable' colspan='{$colspan}'>Attendee Event</th></tr>"
                . "<tr><th>Name</th><th>Role</th><th>Action</th></tr>";

        $statement = $this->conn->runQuery("SELECT attendee.idattendee as id, attendee.name as name, role.name as role from attendee inner join role on attendee.role = role.idrole ", "Attendee");
        $statement->execute();

        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        $dataCount = count($data);
        $i = 1;

        foreach ($data as $row) {
            $resultTable .= "<tr>"
                    . "<td id='user__name_{$i}'>{$row->getName()}</td>"
                    . "<td id='user__role_{$i}'>{$row->getRole()}</td>"
                    . "<td><a href='deleteData?type=deleteUser&user_id={$row->getId()}'>Delete</a></td>"
                    . "</tr>";
            $i = $i + 1;
        }
        $resultTable .= "<tr><td colspan='{$colspan}'>Results found: {$dataCount}</td></tr>"
                . "<tr><td colspan='{$colspan}'><a href='addData.php?type=adminUserHandler'>Add User</a></td></tr>"
                . "</table>";

        return $resultTable;
    }

}
