<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SelectMaker {    
    
    public function renderOptions($options, $selected = ''){
    
        $result = "";
        foreach($options as $row)
        {
            $row_value = $row->getValue();
            $result .= "<option ";
            if($selected != '')
            {
                if($selected === $row_value)
                {
                    $result .= "selected ";
                }
            }
            $result .= "value='{$row->getValue()}'>{$row->getLabel()}</option>";
        }
        return $result;
        
    }
}