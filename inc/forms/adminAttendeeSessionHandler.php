<form name='adminAttendeeSessionHandler' class='form-template' method='POST' action=''>
    Session:
    <select name='adminAttendeeSessionHandler_session'>
        <?php
        $statement = $conn->runQuery("select session.name as label, session.idsession as value from session;","Value_Label");
        $statement->execute();
        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        
        echo $select_maker->renderOptions($data);
        ?>
    </select>
    Session:
    <select name='adminAttendeeSessionHandler_attendee'>
        <?php
        $statement = $conn->runQuery("select attendee.name as label, attendee.idattendee as value from attendee;","Value_Label");
        $statement->execute();
        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        
        echo $select_maker->renderOptions($data);
        ?>
    </select>
    <input type='submit' value='SUBMIT'/>
</form>

