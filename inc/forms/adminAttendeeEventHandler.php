<form name='adminAttendeeEventHandler' class='form-template' method='POST' action=''>
    Event:
    <select name='adminAttendeeEventHandler_attendee'>
        <?php
        $statement = $conn->runQuery("select attendee.idattendee as value, attendee.name as label from attendee ","Value_Label");
        //$statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();
        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        
        echo $select_maker->renderOptions($data);
        ?>
    </select>
    <select name='adminAttendeeEventHandler_event'>
        <?php
        $statement = $conn->runQuery("select event.idevent as value, event.name as label from event ","Value_Label");
        //$statement->bindParam(":userid", $user_id, PDO::PARAM_INT);
        $statement->execute();
        $data = array();
        while ($userrow = $statement->fetch()) {
            $data[] = $userrow;
        }
        
        echo $select_maker->renderOptions($data);
        ?>
    </select>
    Paid:
    <select name='adminAttendeeEventHandler_paid'>
        <option value='1'>Yes</option>
        <option value='0'>No</option>
    </select>
    <input type='submit' value='SUBMIT'/>
</form>

