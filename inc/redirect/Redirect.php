<?php

class Redirect {
    
    /**
     * If the user isn't logged in, redirect back to
     */
    function redirect_to($dir, $message="") {
            $location = "Location: $dir.php";
            if($message !== ""){
                $location .= '?response_message:' . $message;
            }
            header("Location: $dir.php");
            exit; 
    }

}
