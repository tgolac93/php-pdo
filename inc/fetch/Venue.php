<?php
class Venue{
    private $id;
    private $name;
    private $capacity;
    
    public function getId(){
        return $this->id;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getCapacity(){
        return $this->capacity;
    }
}
