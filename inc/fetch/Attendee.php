<?php

class Attendee {
    private $id;
    private $name;
    private $password;
    private $role;

    public function getId(){
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getPassword(){
        return $this->password;
    }

    public function getRole() {
        return $this->role;
    }

}
