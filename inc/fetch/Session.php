<?php
class Session{
    private $id;
    private $name;
    private $numberAllowed;
    private $event;
    private $startDate;
    private $endDate;
    
    public function getId(){
        return $this->id;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getNumberAllowed(){
        return $this->numberAllowed;
    }
    
    public function getEvent(){
        return $this->event;
    }
    
    public function getStartDate(){
        return $this->startDate;
    }
    
    public function getEndDate(){
        return $this->endDate;
    }
    
}
