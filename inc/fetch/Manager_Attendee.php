<?php
class Manager_Attendee{
    private $eventId;
    private $id;
    private $name;
    private $role;
    private $paid;
    private $event;
    
    public function getEventId(){
        return $this->eventId;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getPaid(){
        $result = "Yes";
        if($this->paid == 0){
         $result = "No";
        }
        return $result;
    }

    public function getRole() {
        return $this->role;
    }
    
    public function getEvent(){
        return $this->event;
    }
}