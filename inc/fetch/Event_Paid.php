<?php

class Event_Paid {

    private $id;
    private $name;
    private $dateStart;
    private $dateEnd;
    private $numberAllowed;
    private $venue;
    private $paid;

    public function getId() {
        return $this->id;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getDateStart(){
        return $this->dateStart;
    }
    
    public function getDateEnd(){
        return $this->dateEnd;
    }
    
    public function getNumberAllowed(){
        return $this->numberAllowed;
    }
    
    public function getVenue(){
        return $this->venue;
    }
    
    public function getPaid(){
        $result = "Yes";
        if($this->paid == 0){
         $result = "No";
        }
        return $result;
    }

}
