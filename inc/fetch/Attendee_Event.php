<?php

Class Attendee_Event{
    private $event;
    private $attendee;
    private $paid;
    
    public function getEvent(){
        return $this->event;
    }
    
    public function getAttendee(){
        return $this->attendee;
    }
    
    public function getPaid(){
        $result = "Yes";
        if($this->paid == 0){
         $result = "No";
        }
        return $result;
    }
}