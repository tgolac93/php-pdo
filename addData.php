<?php
session_start();
include 'inc/redirect/Redirect.php';
include 'inc/template_parts/Header.php';
include 'inc/template_parts/Footer.php';
include 'inc/conn/DBConn.php';
include 'settings.config';
include 'inc/dynamic/SelectMaker.php';
$conn = new DBConn($conf);
$username = (string) $_SESSION['username'];
$role = intval((string) $_SESSION['role']);
$user_id = intval((string) $_SESSION['user_id']);
$page_name = "Add Data";

$select_maker = new SelectMaker();
$redirect = new Redirect();
$error = false;

if (!isset($_SESSION['login']) || $_SESSION['login'] == "") {
    $redirect->redirectTo("login");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Data</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <?php
        $header = new Header($role, $username, $page_name);
        echo $header->render();
        ?>
        <main>

            <?php
            if ($_GET['type'] == 'attendeeSessionHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $session_id = intval(filter_var($_POST['attendeeSessionHandler_session'], FILTER_SANITIZE_NUMBER_INT));
                    $input_userid = intval(filter_var($user_id, FILTER_SANITIZE_NUMBER_INT));
                    try {
                        $statement = $conn->runQuery('INSERT INTO `attendee_session` (`session`, `attendee`) VALUES (:session, :attendee);');
                        $statement->bindParam(':session', $session_id, PDO::PARAM_INT);
                        $statement->bindParam(':attendee', $input_userid, PDO::PARAM_INT);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('registrations');
                    }
                }
                ?>
                <section>

                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Attendee_Session.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/attendeeSessionHandler.php';
                    ?>
                </section><?php
            }
            ?>
            <?php
            if ($_GET['type'] == 'attendeeEventHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $event_id = intval(filter_var($_POST['attendeeEventHandler_event'], FILTER_SANITIZE_NUMBER_INT));
                    $paid = intval(filter_var($_POST['attendeeEventHandler_paid'], FILTER_SANITIZE_NUMBER_INT));
                    $input_userid = intval(filter_var($user_id, FILTER_SANITIZE_NUMBER_INT));
                    try {
                        $statement = $conn->runQuery('INSERT INTO `attendee_event` (`event`, `attendee`,`paid`) VALUES (:event, :attendee, :paid);');
                        $statement->bindParam(':event', $event_id, PDO::PARAM_STR);
                        $statement->bindParam(':attendee', $input_userid, PDO::PARAM_STR);
                        $statement->bindParam(':paid', $paid, PDO::PARAM_STR);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('registrations');
                    }
                }
                ?>

                <section>

                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Attendee_Event.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/attendeeEventHandler.php';
                    ?>
                </section><?php
            }
            ?>

            <?php
            if ($_GET['type'] == 'managerEventHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $name = filter_var($_POST['managerEventHandler_name'], FILTER_SANITIZE_STRING);
                    $stdate = filter_var($_POST['managerEventHandler_startdate'], FILTER_SANITIZE_STRING);
                    $endate = filter_var($_POST['managerEventHandler_enddate'], FILTER_SANITIZE_STRING);
                    $num = intval(filter_var($_POST['managerEventHandler_numberAllowed'], FILTER_SANITIZE_NUMBER_INT));
                    $venue = intval(filter_var($_POST['managerEventHandler_venue'], FILTER_SANITIZE_NUMBER_INT));

                    try {
                        $statement = $conn->runQuery("INSERT INTO `event` (`idevent`, `name`, `datestart`, `dateend`, `numberallowed`, `venue`) VALUES (NULL, :name , :start , :end , :num , :venue);");
                        $statement->bindParam(':name', $name, PDO::PARAM_STR);
                        $statement->bindParam(':start', $stdate, PDO::PARAM_STR);
                        $statement->bindParam(':end', $endate, PDO::PARAM_STR);
                        $statement->bindParam(':num', $num, PDO::PARAM_INT);
                        $statement->bindParam(':venue', $venue, PDO::PARAM_INT);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('registrations');
                    }
                }
                ?>
                <section>
                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Venue.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/managerEventHandler.php';
                    ?>
                </section><?php } ?>

            <?php
            if ($_GET['type'] == 'managerAttendeeHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $event_id = intval(filter_var($_POST['managerAttendeeHandler_event'], FILTER_SANITIZE_NUMBER_INT));
                    $paid = intval(filter_var($_POST['managerAttendeeHandler_paid'], FILTER_SANITIZE_NUMBER_INT));
                    $input_userid = intval(filter_var($_POST['managerAttendeeHandler_attendee'], FILTER_SANITIZE_NUMBER_INT));
                    try {
                        $statement = $conn->runQuery('INSERT INTO `attendee_event` (`event`, `attendee`,`paid`) VALUES (:event, :attendee, :paid);');
                        $statement->bindParam(':event', $event_id, PDO::PARAM_STR);
                        $statement->bindParam(':attendee', $input_userid, PDO::PARAM_STR);
                        $statement->bindParam(':paid', $paid, PDO::PARAM_STR);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>
                <section>
                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Venue.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/managerAttendeeHandler.php';
                    ?>
                </section><?php } ?>

            <?php
            if ($_GET['type'] == 'managerSessionHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $name = filter_var($_POST['managerSessionHandler_name'], FILTER_SANITIZE_STRING);
                    $stdate = filter_var($_POST['managerSessionHandler_startdate'], FILTER_SANITIZE_STRING);
                    $endate = filter_var($_POST['managerSessionHandler_enddate'], FILTER_SANITIZE_STRING);
                    $num = intval(filter_var($_POST['managerSessionHandler_numberAllowed'], FILTER_SANITIZE_NUMBER_INT));
                    $event = intval(filter_var($_POST['managerSessionHandler_event'], FILTER_SANITIZE_NUMBER_INT));

                    try {
                        $statement = $conn->runQuery("INSERT INTO `session` (`idsession`, `name`, `numberallowed`, `event`, `startdate`, `enddate`) VALUES "
                                . "(NULL, :name, :num, :event, :start, :end);");
                        $statement->bindParam(':name', $name, PDO::PARAM_STR);
                        $statement->bindParam(':start', $stdate, PDO::PARAM_STR);
                        $statement->bindParam(':end', $endate, PDO::PARAM_STR);
                        $statement->bindParam(':num', $num, PDO::PARAM_INT);
                        $statement->bindParam(':event', $event, PDO::PARAM_STR);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>
                <section>
                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Venue.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/managerSessionHandler.php';
                    ?>
                </section><?php }
                ?>

            <?php
            if ($_GET['type'] == 'adminUserHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $name = filter_var($_POST['adminUserHandler_name'], FILTER_SANITIZE_STRING);
                    $pass = hash('sha256', filter_var($_POST['adminUserHandler_password'], FILTER_SANITIZE_STRING));
                    $role = intval(filter_var($_POST['adminUserHandler_role'], FILTER_SANITIZE_NUMBER_INT));
                    try {
                        $statement = $conn->runQuery("INSERT INTO `attendee`"
                                . "(`name`, `password`, `role`) "
                                . "VALUES ( :name, :pass, :role);");
                        $statement->bindParam(':name', $name, PDO::PARAM_STR);
                        $statement->bindParam(':pass', $pass, PDO::PARAM_STR);
                        $statement->bindParam(':role', $role, PDO::PARAM_INT);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>
                <section>
                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Venue.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/adminUserHandler.php';
                    ?>
                </section><?php }
                ?>

            <?php
            if ($_GET['type'] == 'adminVenueHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $name = filter_var($_POST['adminVenueHandler_name'], FILTER_SANITIZE_STRING);
                    $capacity = intval(filter_var($_POST['adminVenueHandler_capacity'], FILTER_SANITIZE_NUMBER_INT));
                    try {
                        $statement = $conn->runQuery("INSERT INTO `venue` (`name`, `capacity`) VALUES (:name, :capacity) ");
                        $statement->bindParam(':name', $name, PDO::PARAM_STR);
                        $statement->bindParam(':capacity', $capacity, PDO::PARAM_INT);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>
                <section>
                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Venue.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/adminVenueHandler.php';
                    ?>
                </section><?php }
                ?>
            
            <?php
            if ($_GET['type'] == 'adminAttendeeSessionHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $session_id = intval(filter_var($_POST['adminAttendeeSessionHandler_session'], FILTER_SANITIZE_NUMBER_INT));
                    $input_userid = intval(filter_var($_POST['adminAttendeeSessionHandler_attendee'], FILTER_SANITIZE_NUMBER_INT));
                    try {
                        $statement = $conn->runQuery('INSERT INTO `attendee_session` (`session`, `attendee`) VALUES (:session, :attendee);');
                        $statement->bindParam(':session', $session_id, PDO::PARAM_INT);
                        $statement->bindParam(':attendee', $input_userid, PDO::PARAM_INT);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>
                <section>

                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Attendee_Session.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/adminAttendeeSessionHandler.php';
                    ?>
                </section><?php
            }
            ?>
            
            <?php
            if ($_GET['type'] == 'adminAttendeeEventHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $event_id = intval(filter_var($_POST['adminAttendeeEventHandler_event'], FILTER_SANITIZE_NUMBER_INT));
                    $paid = intval(filter_var($_POST['adminAttendeeEventHandler_paid'], FILTER_SANITIZE_NUMBER_INT));
                    $input_userid = intval(filter_var($_POST['adminAttendeeEventHandler_attendee'], FILTER_SANITIZE_NUMBER_INT));
                    
                    try {
                        $statement = $conn->runQuery('INSERT INTO `attendee_event` (`event`, `attendee`,`paid`) VALUES (:event, :attendee, :paid);');
                        $statement->bindParam(':event', $event_id, PDO::PARAM_INT);
                        $statement->bindParam(':attendee', $input_userid, PDO::PARAM_INT);
                        $statement->bindParam(':paid', $paid, PDO::PARAM_INT);
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>

                <section>

                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Attendee_Event.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/adminAttendeeEventHandler.php';
                    ?>
                </section><?php
            }
            ?>
            
            <?php
            if ($_GET['type'] == 'adminManagerEventHandler') {
                $error = false;
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $event_id = intval(filter_var($_POST['adminManagerEventHandler_event'], FILTER_SANITIZE_NUMBER_INT));
                    $input_userid = intval(filter_var($_POST['adminManagerEventHandler_manager'], FILTER_SANITIZE_NUMBER_INT));
                    
                    try {
                        $statement = $conn->runQuery('INSERT INTO `manager_event` (`event`, `manager`) VALUES (:event, :manager);');
                        $statement->bindParam(':event', $event_id, PDO::PARAM_INT);
                        $statement->bindParam(':manager', $input_userid, PDO::PARAM_INT);
                        
                        $statement->execute();
                    } catch (PDOException $ex) {
                        $error = true;
                        $errorMessage = $ex->getMessage();
                    }

                    if (!$error) {
                        $redirect->redirect_to('admin');
                    }
                }
                ?>

                <section>

                    <h2>insert session</h2>
                    <span class='error'><?php
                        if ($error) {
                            echo $errorMessage;
                        }
                        ?></span>
                    <?php
                    include 'inc/fetch/Attendee_Event.php';
                    include 'inc/fetch/Value_Label.php';
                    include 'inc/forms/adminManagerEventHandler.php';
                    ?>
                </section><?php
            }
            ?>
        </main>
        <?php
        $footer = new Footer();
        echo $footer->render();
        ?>
    </body>

</html>

