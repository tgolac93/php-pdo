<?php

session_start();
include 'inc/redirect/Redirect.php';
include 'inc/template_parts/Header.php';
include 'inc/template_parts/Footer.php';
include 'inc/conn/DBConn.php';
include 'settings.config';
include 'inc/dynamic/SelectMaker.php';
$conn = new DBConn($conf);
$username = (string) $_SESSION['username'];
$role = intval((string) $_SESSION['role']);
$user_id = intval((string) $_SESSION['user_id']);
$page_name = "Add Data";

$select_maker = new SelectMaker();
$redirect = new Redirect();
$error = false;

if (!isset($_SESSION['login']) || $_SESSION['login'] == "") {
    $redirect->redirectTo("login");
}
if ($_GET['type'] == 'attendeeSession') {
    $session = intval(filter_var($_GET['session'], FILTER_SANITIZE_NUMBER_INT));
    $user = intval(filter_var($_GET['user'], FILTER_SANITIZE_NUMBER_INT));
    try {
        $statement = $conn->runQuery("DELETE FROM `attendee_session` WHERE `attendee_session`.`session` = :session AND `attendee_session`.`attendee` = :user");
        $statement->bindParam(':session', $session, PDO::PARAM_INT);
        $statement->bindParam(':user', $user, PDO::PARAM_INT);
        $statement->execute();
    } catch (PDOException $ex) {
        $error = true;
        $errorMessage = $ex->getMessage();
    }
    $redirect->redirect_to('registrations');
} elseif ($_GET['type'] == 'attendeeEvent') {
    $event = intval(filter_var($_GET['event'], FILTER_SANITIZE_NUMBER_INT));
    $user = intval(filter_var($_GET['user'], FILTER_SANITIZE_NUMBER_INT));
    try {
        $statement = $conn->runQuery("DELETE FROM `attendee_event` WHERE `attendee_event`.`event` = :event AND `attendee_event`.`attendee` = :user");
        $statement->bindParam(':event', $event, PDO::PARAM_INT);
        $statement->bindParam(':user', $user, PDO::PARAM_INT);
        $statement->execute();
    } catch (PDOException $ex) {
        $error = true;
        $errorMessage = $ex->getMessage();
    }
    $redirect->redirect_to('registrations');
} elseif ($_GET['type'] == 'eventDelete') {
    $event = intval(filter_var($_GET['event_id'], FILTER_SANITIZE_NUMBER_INT));

    try {
        $statement = $conn->runQuery("DELETE FROM `event` WHERE `event`.`idevent` = :event");
        $statement->bindParam(':event', $event, PDO::PARAM_INT);

        $statement->execute();
    } catch (PDOException $ex) {
        $error = true;
        $errorMessage = $ex->getMessage();
        echo $errorMessage;
    }
    $redirect->redirect_to('admin');
} elseif ($_GET['type'] == 'deleteSession') {
    $session = intval(filter_var($_GET['session_id'], FILTER_SANITIZE_NUMBER_INT));

    try {
        $statement = $conn->runQuery("DELETE FROM `session` WHERE `session`.`idsession` = :session");
        $statement->bindParam(':session', $session, PDO::PARAM_INT);

        $statement->execute();
    } catch (PDOException $ex) {
        $error = true;
        $errorMessage = $ex->getMessage();
        echo $errorMessage;
    }
    $redirect->redirect_to('admin');
} elseif ($_GET['type'] == 'deleteUser') {
    $user = intval(filter_var($_GET['user_id'], FILTER_SANITIZE_NUMBER_INT));
    if ($user <= 1) {
        $redirect->redirect_to('admin');
        echo 'kill';
    } else {
        try {
            $statement = $conn->runQuery("DELETE FROM `attendee` WHERE `attendee`.`idattendee` = :user");
            $statement->bindParam(':user', $user, PDO::PARAM_INT);

            $statement->execute();
        } catch (PDOException $ex) {
            $error = true;
            $errorMessage = $ex->getMessage();
            echo $errorMessage;
        }
        $redirect->redirect_to('admin');
    }
}
?>



